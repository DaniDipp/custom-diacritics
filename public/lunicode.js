function Lunicode() {
    this.creepify =  {
        init: function () {

            // Sort diacritics in top, bottom or middle

            for (var i = 768; i <= 789; i++) {
                this.diacriticsTop.push(String.fromCharCode(i));
            }

            for (var i = 790; i <= 819; i++) {
                if (i != 794 && i != 795) {
                    this.diacriticsBottom.push(String.fromCharCode(i));
                }
            }
            this.diacriticsTop.push(String.fromCharCode(794));
            this.diacriticsTop.push(String.fromCharCode(795));

            for (var i = 820; i <= 824; i++) {
                this.diacriticsMiddle.push(String.fromCharCode(i));
            }

            for (var i = 825; i <= 828; i++) {
                this.diacriticsBottom.push(String.fromCharCode(i));
            }

            for (var i = 829; i <= 836; i++) {
                this.diacriticsTop.push(String.fromCharCode(i));
            }
            this.diacriticsTop.push(String.fromCharCode(836));
            this.diacriticsBottom.push(String.fromCharCode(837));
            this.diacriticsTop.push(String.fromCharCode(838));
            this.diacriticsBottom.push(String.fromCharCode(839));
            this.diacriticsBottom.push(String.fromCharCode(840));
            this.diacriticsBottom.push(String.fromCharCode(841));
            this.diacriticsTop.push(String.fromCharCode(842));
            this.diacriticsTop.push(String.fromCharCode(843));
            this.diacriticsTop.push(String.fromCharCode(844));
            this.diacriticsBottom.push(String.fromCharCode(845));
            this.diacriticsBottom.push(String.fromCharCode(846));
            // 847 (U+034F) is invisible http://en.wikipedia.org/wiki/Combining_grapheme_joiner
            this.diacriticsTop.push(String.fromCharCode(848));
            this.diacriticsTop.push(String.fromCharCode(849));
            this.diacriticsTop.push(String.fromCharCode(850));
            this.diacriticsBottom.push(String.fromCharCode(851));
            this.diacriticsBottom.push(String.fromCharCode(852));
            this.diacriticsBottom.push(String.fromCharCode(853));
            this.diacriticsBottom.push(String.fromCharCode(854));
            this.diacriticsTop.push(String.fromCharCode(855));
            this.diacriticsTop.push(String.fromCharCode(856));
            this.diacriticsBottom.push(String.fromCharCode(857));
            this.diacriticsBottom.push(String.fromCharCode(858));
            this.diacriticsTop.push(String.fromCharCode(859));
            this.diacriticsBottom.push(String.fromCharCode(860));
            this.diacriticsTop.push(String.fromCharCode(861));
            this.diacriticsTop.push(String.fromCharCode(861));
            this.diacriticsBottom.push(String.fromCharCode(863));
            this.diacriticsTop.push(String.fromCharCode(864));
            this.diacriticsTop.push(String.fromCharCode(865));
			
			if(localStorage.options){
				this.options = JSON.parse(localStorage.options);
			} else {
				this.options = {
					top: true,
					middle: true,
					bottom: true,
					maxHeight: 5, // How many diacritic marks shall we put on top/bottom?
					randomization: 50, // 0-100%. maxHeight 100 and randomization 20%: the height goes from 80 to 100. randomization 70%, height goes from 30 to 100.
					horiz: 50
				}
				localStorage.options = JSON.stringify(this.options);
			}
        },

        encode: function (text) {
            var newText = '',
            newChar;
            for (i in text) {
                newChar = text[i];

                // Middle
                // Put just one of the middle characters there, or it gets crowded
                if (this.options.middle) {
                    newChar += this.diacriticsMiddle[Math.floor(Math.random() * this.diacriticsMiddle.length)]
                }

                // Top
                if (this.options.top) {

                    // Put up to this.options.maxHeight random diacritics on top.
                    // optionally fluctuate the number via the randomization value (0-100%)
                    // randomization 100%: 0 to maxHeight
                    //                30%: 70% of maxHeight to maxHeight
                    //                 x%: 100-x% of maxHeight to maxHeight
                    var diacriticsTopLength = this.diacriticsTop.length - 1;
                    for (var count = 0,
                        len = this.options.maxHeight - Math.random() * ((this.options.randomization / 100) * this.options.maxHeight); count < len; count++) {

                        newChar += this.diacriticsTop[Math.floor(Math.random() * diacriticsTopLength)]

                    }

                }

                // Bottom
                if (this.options.bottom) {

                    var diacriticsBottomLength = this.diacriticsBottom.length - 1;
                    for (var count = 0,
                        len = this.options.maxHeight - Math.random() * ((this.options.randomization / 100) * this.options.maxHeight); count < len; count++) {

                        newChar += this.diacriticsBottom[Math.floor(Math.random() * diacriticsBottomLength)]

                    }

                }

                newText += newChar;
            }
            return newText;
        },

        decode: function (text) {
            var newText = '',
            charCode;

            for (i in text) {
                charCode = text[i].charCodeAt(0);
                if (charCode < 768 || charCode > 865) {
                    newText += text[i];
                }
            }
            return newText;
        },

        diacriticsTop: [],
        diacriticsMiddle: [],
        diacriticsBottom: [],

    };

    // init
    for (i in this.tools) {
        this.tools[i].init();
    }

    // Encode every character: U+00A0 -> &#x00a0; etc.
    this.getHTML = function (text) {
        var html = '',
        ch,
        lastSpaceWasNonBreaking = true, // for alternating [non-braking] spaces
        highSurrogate = 0,
        codepoint = 0;

        for (var i = 0, len = text.length; i < len; i++) {
            ch = text.charCodeAt(i);

            // line break: add <br>\n
            if (ch == 10 || ch == 13) {
                html += '<br>\n';
                lastSpaceWasNonBreaking = true;

                // space: add alternating space and non-breaking space (U+00A0). Otherwise
                // a series of normal spaces       would collapse to one in the browser
            } else if (ch == 32) {
                if (lastSpaceWasNonBreaking) {
                    html += ' ';
                    lastSpaceWasNonBreaking = false;
                } else {
                    html += '&nbsp;';
                    lastSpaceWasNonBreaking = true;
                }

                // Normal character: Decode. Special cases for higher numbers:
                // http://en.wikipedia.org/wiki/Mapping_of_Unicode_characters#Surrogates
            } else {

                // Character is high surrogate: Remember and continue
                if (ch >= 0xD800 && ch <= 0xDBFF) {
                    highSurrogate = ch;
                    codepoint = 0;

                    // last character was high surrogate: Combine with low surrogate
                } else if (highSurrogate > 0) {

                    // If char is low surrogate:
                    if (ch >= 0xDC00 && ch <= 0xDFFF) {
                        codepoint = (highSurrogate - 0xD800) * 1024 + (ch - 0xDC00) + 0x10000;
                    }
                    highSurrogate = 0;

                    // no surrogates: Just take the character
                } else {
                    codepoint = ch;
                }

                if (codepoint != 0) {
                    html += '&#x' + codepoint.toString(16) + ';';
                    lastSpaceWasNonBreaking = true;
                }

            }
        }

        return html;
    }
}